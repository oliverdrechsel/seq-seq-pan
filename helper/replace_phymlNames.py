# this tool is meant to use a tab delimited file of numbers \t names
# and replace the respective numbers in an phyml output with the names

import argparse
import pprint
import re

parser = argparse.ArgumentParser(description= "Rename samples in phyml output using a tab delimited translation file.")
parser.add_argument('--input', '-i',
                    dest='input',
                    help=("phyml output file"),
                    required=True,
                    type=argparse.FileType('r'))
parser.add_argument('--output', '-o',
                    dest='output',
                    help=("phyml file with numbers replaced by names"),
                    required=True,
                    type=argparse.FileType('w'))
parser.add_argument('--translate', '-t',
                    dest='translate',
                    help=("tab delimited file relating numbers to names"),
                    required=True,
                    type=argparse.FileType('r'))

arguments = parser.parse_args()

DEBUG = pprint.PrettyPrinter(indent=4).pprint

transDict = dict()

# clean up genome names
# avoid ["\s", ",", ":", ";", "(", ")", "[", "]"]
illegal = re.compile('([\s,:;\(\)\[\]])')
consecutive = re.compile('_+')
trailing = re.compile('_$')

for line in arguments.translate:
    line = line.rstrip()

    # clean up illegal characters
    splitline = line.split("\t")
    splitline[1] = illegal.sub("_", splitline[1])
    splitline[1] = consecutive.sub("_", splitline[1]) # cleaning produces consecutive underscores -> remove
    splitline[1] = trailing.sub("_", splitline[1]) # trailing underscores also suck -> remove
    transDict[splitline[0]] = splitline[1]

# there are two possible locations where the number needs to be replaced by a name
# (1: or ,1: would both be a valid location
# ,1.000000: should not be replaced


treeContent = arguments.input.readline()

for k in transDict.keys():
    # look for the first pattern "(1:"
    target = "(" + k + ":"
    replac = "(" + transDict[k] + ":"

    treeContent = treeContent.replace(target, replac)

    # look for the second pattern ",1:"
    target = "," + k + ":"
    replac = "," + transDict[k] + ":"

    treeContent = treeContent.replace(target, replac)

arguments.output.write(treeContent)